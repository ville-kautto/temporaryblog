
export interface Blog {
  title: string;
  content: string;
  author: string;
  timeActive: number;
  createdAt?: Date;
  expiresAt?: Date;
}
