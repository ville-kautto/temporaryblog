import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Blog} from "../../models/Blog";
import {Observable} from "rxjs";
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  private baseUrl = environment.apiBaseUrl;

  fetchBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(`${this.baseUrl}/api/blogs`)
  }

  createBlog(blog: Blog): Observable<Blog> {
    return this.http.post<Blog>(`${this.baseUrl}/api/blogs`, blog)
  }
}
