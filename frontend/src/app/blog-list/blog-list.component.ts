import { Component, OnInit } from '@angular/core';
import {Blog} from "../shared/models/Blog";
import {BlogService} from "../shared/services/BlogService/blog.service";

@Component({
  selector: 'blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

  blogs: Blog[] = [];

  constructor(private blogService: BlogService) { }

  ngOnInit(): void {
    console.log(this.blogService.fetchBlogs())
    this.blogService.fetchBlogs().subscribe(
      blogs => this.blogs = blogs
    );
  }

}
