package xyz.kautto.TemporaryBlog.service;

import org.springframework.stereotype.Service;
import xyz.kautto.TemporaryBlog.entity.Blog;

@Service
public class BlogValidator {

    // Returns true if all fields are valid, otherwise returns false
    public boolean validate(Blog blog) {
        if(!validateTitle(blog.getTitle())) { return false; }
        if(!validateContent(blog.getContent())) { return false; }
        if(!validateAuthor(blog.getAuthor())) { return false; }
        if(!validateTimeActive(blog.getTimeActive())) { return false; }
        return true;
    }

    private boolean validateTitle(String title) {
        if(title.length() > 0 && title.length() < 64) {
            return true;
        }
        return false;
    }

    private boolean validateContent(String content) {
        if(content.length() > 0 && content.length() < 10000) {
            return true;
        }
        return false;
    }
    private boolean validateAuthor(String author) {
        if(author.length() > 0 && author.length() < 32) {
            return true;
        }
        return false;
    }
    private boolean validateTimeActive(Long timeActive) {
        if(timeActive > 0 && timeActive <= 10080) {
            return true;
        }
        return false;
    }
}
