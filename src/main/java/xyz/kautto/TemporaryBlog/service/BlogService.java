package xyz.kautto.TemporaryBlog.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.kautto.TemporaryBlog.controller.BlogController;
import xyz.kautto.TemporaryBlog.entity.Blog;
import xyz.kautto.TemporaryBlog.repository.BlogRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BlogService {

    public final BlogRepository blogRepo;
    public final BlogValidator blogValidator;
    Logger logger = LoggerFactory.getLogger(BlogService.class);

    @Autowired
    public BlogService(BlogRepository blogRepo, BlogValidator blogValidator) {
        this.blogRepo = blogRepo;
        this.blogValidator = blogValidator;
    }

    public List<Blog> fetchBlogs() {
        return blogRepo.findAllByOrderByIdDesc();
    }

    public Blog createBlog(Blog blog) {
        if(this.blogValidator.validate(blog)) {
            blogRepo.save(blog);
            return blog;
        }
        logger.error("Given blog was invalid!");
        return blog;
    }

    @Transactional
    public void deleteExpired() {
        LocalDateTime time = java.time.LocalDateTime.now();
        blogRepo.deleteByExpiresAtBefore(time);
    }
}
