package xyz.kautto.TemporaryBlog.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.kautto.TemporaryBlog.entity.Blog;

import java.time.LocalDateTime;
import java.util.List;

public interface BlogRepository extends CrudRepository<Blog, Long> {
    List<Blog> findAllByOrderByIdDesc();

    void deleteByExpiresAtBefore(LocalDateTime expiryDate);
}
