package xyz.kautto.TemporaryBlog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.kautto.TemporaryBlog.entity.Blog;
import xyz.kautto.TemporaryBlog.service.BlogService;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api")
public class BlogController {

    // BlogService controls the delivery of information from and to the database
    BlogService blogService;

    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }

    Logger logger = LoggerFactory.getLogger(BlogController.class);

    @GetMapping("/blogs")
    @ResponseStatus(ACCEPTED)
    public List<Blog> getBlogs() {
        blogService.deleteExpired();
        logger.info("Returning blogs");
        return blogService.fetchBlogs();
    }

    @PostMapping("/blogs")
    @ResponseStatus(CREATED)
    public ResponseEntity<Blog> createBlog(@RequestBody Blog blog) {
        LocalDateTime time = java.time.LocalDateTime.now();
        blog.setCreatedAt(time);
        blog.setExpiresAt(setExpirationTime(time, blog.getTimeActive()));
        logger.info(String.format("Blog valid for %s - %s ", blog.getCreatedAt(), blog.getExpiresAt()));
        Blog newBlog = blogService.createBlog(blog);
        return new ResponseEntity<>(newBlog, HttpStatus.OK);
    }

    private LocalDateTime setExpirationTime(LocalDateTime time, Long timeActive) {
        return time.plusMinutes(timeActive);
    }

}
