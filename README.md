# TemporaryBlogJava

A blog site where everybody can post and no messages will remain forever.
This is the java version of a simple web application, the kotlin version of the application can be found [here](https://gitlab.com/ville-kautto/temporaryblogkotlin)

### Requirements
Running the application requires the following programs installed into your system
- Java 8 or later

## Building the application
To build the application, clone the repository and run the following command in the project's root directory.
> mvnw clean install

## Running the application
To start the application, run the following command after building the application.
> mvnw spring-boot:run

The application will then be started and the application can be accessed with a web browser at localhost:8080
